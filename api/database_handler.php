<?php
namespace Cygge\Recipes;

/**
 * Class for handling database queries, built as a collection of static functions
 * to be used as a singleton.
 */
class DatabaseHandler {
    
    protected static $dbLink;

    
    /**
     * Function to initiate the database connection if none exists. Safe to
     * call multiple times. 
     */
    protected static function createConnection() {
        if (!static::$dbLink) {
            static::$dbLink = mysqli_connect($GLOBALS['recipeDB']['sqlServer'], $GLOBALS['recipeDB']['sqlUsername'], $GLOBALS['recipeDB']['sqlPassword']);
            mysqli_select_db(static::$dbLink, $GLOBALS['recipeDB']['sqlDatabaseName']);
        }
    }
    
    
    /**
     * Perform a database query
     * 
     * @param string $query The SQL-query to perform
     * @return \mysqli_result
     */
    public static function query($query) {
        self::createConnection();
        return mysqli_query(static::$dbLink, $query);
    }
    
    
    /**
     * Escape and insert a key/value map into the given table name.
     * 
     * @param string $tableName Name of the table to insert values in.
     * @param array $values Array of key/value-pairs to insert. Will be escaped automatically.
     * @return \mysqli_result
     */
    public static function insert($tableName, $values) {
        $query = "INSERT INTO `". self::escape($tableName)."` SET ";
        $valueMap = Array();
        foreach ($values as $key => $value) {
            $valueMap[] = "`". self::escape($key) . "` = '" . self::escape($value) . "'";
        }
        if (count($valueMap)) {
            $query .= implode(", ", $valueMap);
            return self::query($query);
        } else {
            return false;
        }
    }
    
    
    
    /**
     * Escape a string for SQL-usage
     * 
     * @param string $string The string to escape
     * @return string
     */
    public static function escape($string) {
        self::createConnection();
        return mysqli_escape_string(static::$dbLink, $string);
    }

    
    
    /**
     * Get an associative array from an SQL query string
     * 
     * @param string $query The SQL-query to perform
     * @return array
     */
    public static function getArrayFromQuery($query) {
        $queryResult = self::query($query);
        return self::getArrayFromResult($queryResult);
    }
    
    
    /**
     * Get an array of result data from given result
     * 
     * @param \mysqli_result $result The result resource to read from
     * @return array
     */
    public static function getArrayFromResult($result) {
        $return = Array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($return, $row);
        }
        return $return;
    }
    
    
    
}
