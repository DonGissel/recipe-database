<?php

// Configuration file for the win!
require '_config.php';

// We'll need our database handler.
require 'database_handler.php';

// Get Slim and all it's dependencies set up from Composer packages.
require '../vendor/autoload.php';

// Set up the app framework using Slim
$app = new \Slim\App;

// Load the various REST endpoints we'll need.
require "app/measurement.php";
require "app/ingredient.php";
require "app/recipe.php";

// Has anyone really been far even as decided to use even go want to do look more like?
$app->run();