<?php
namespace Cygge\Recipes;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;



$app->get('/ingredients', function (Request $request, Response $response) {
    $list = DatabaseHandler::getArrayFromQuery("SELECT * FROM ingredients ORDER BY name ASC");
    $response->getBody()->write(json_encode($list));
    return $response->withHeader('Content-type', 'application/json');
});




$app->put('/ingredient/{name}/{unit}', function (Request $request, Response $response) {
    DatabaseHandler::insert("ingredients", Array(
        "name" => $request->getAttribute("name"),
        "unit" => $request->getAttribute("unit")
    ));
    $response->getBody()->write("OK");
    return $response;
});


