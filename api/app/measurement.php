<?php
namespace Cygge\Recipes;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/measurements', function (Request $request, Response $response) {
    $list = DatabaseHandler::getArrayFromQuery("SELECT * FROM measurements ORDER BY name ASC");
    $response->getBody()->write(json_encode($list));
    return $response;
});


